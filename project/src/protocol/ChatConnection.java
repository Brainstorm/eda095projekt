package protocol;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * I don't know if this is class is needed anymore...
 * But I'll keep it. I thought that maybe some shared state could reside
 * here. But I opted not to make turn this into some big stateful mess.
 * 
 * Originally the receiver and sender had a reference to their connection.
 * But it was unused so I removed it for the time being.
 * 
 * @author Daniel
 *
 */
public class ChatConnection {
	
	private ChatReceiver receiver;
	private ChatSender sender;
	
	public ChatConnection(InputStream in, OutputStream out) {
		receiver = new StreamReceiver(in);
		sender = new StreamSender(out);
		preloadMessageClasses();
	}
	
	private void preloadMessageClasses() {
		String[] messageClasses = new String[] {
				"protocol.messages.AudioData",
				"protocol.messages.CallConnect",
				"protocol.messages.CallHangup",
				"protocol.messages.CallPickup",
				"protocol.messages.Handshake",
				"protocol.messages.IncomingCall",
				"protocol.messages.TextMessage",
				"protocol.messages.User",
				"protocol.messages.UserListReply",
				"protocol.messages.UserListRequest"
		};
		for (String className : messageClasses) {
			try {
				Class.forName(className);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}			
		}
	}

	public ChatReceiver getReceiver() {
		return receiver;
	}
	
	public ChatSender getSender() {
		return sender;
	}
	
}
