package protocol;

import java.io.IOException;

import protocol.messages.Message;

/**
 * Public interface for sending a stream of messages.
 * 
 * @author Daniel
 *
 */
public interface ChatSender {

	/**
	 * Sends a message out on the connection.
	 * Note: Generally not thread safe. 
	 * 
	 * @param m The Message to send
	 * @throws IOException If an IO exception occurs when writing.
	 * 
	 */
	public abstract void sendMessage(Message m) throws IOException;

}