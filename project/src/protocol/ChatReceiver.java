package protocol;

import java.io.IOException;

import protocol.messages.Message;

/**
 * Public interface for receiving a stream of messages.
 * 
 * @author Daniel
 *
 */
public interface ChatReceiver {
	
	/**
	 * Receive a message from the stream of messages.
	 * Note: Generally not thread safe.
	 * 
	 * @return a message from the stream.
	 * @throws IOException if an IO exception occurs when reading.
	 * @throws UnknownMessageException If an unknown message type is received.
	 */
	public abstract Message receive() throws IOException,
			UnknownMessageException;

}