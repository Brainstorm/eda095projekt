package protocol;

public class UnknownMessageException extends Exception {

	private short type;
	
	public UnknownMessageException(short type) {
		super("Unknown Message type: " + type);
		this.type = type; 
	}
	
	public short getType() {
		return type;
	}
	
	private static final long serialVersionUID = -8772709583781741251L;

}
