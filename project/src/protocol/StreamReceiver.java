package protocol;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import protocol.messages.Message;
import protocol.messages.Serializer;

/**
 * Receiver class that handles the receiving of messages.
 * @author Daniel
 */

class StreamReceiver implements ChatReceiver, Serializer {
	private boolean debug = true;
	
	private DataInputStream in;
	
	public StreamReceiver(InputStream in) {
		this.in = new DataInputStream(new BufferedInputStream(in));
	}
	
	// // Public API // // 
	
	/* (non-Javadoc)
	 * @see protocol.ChatReceiver#receive()
	 */
	@Override
	public Message receive() throws IOException, UnknownMessageException {
		// Read message type
		short type = in.readShort();
		// Read message length
		int len = in.readInt();
		// Mark this position in the stream in case something goes wrong.
		in.mark(len);
		// Need this for a Tricky exception priority thing.
		boolean finallyThrow = true;
		try {
			// Find message class
			Class<? extends Message> clazz = Message.getMessageClass(type);
			// If message class is not found then skip length bytes
			if(clazz == null) {
				throw new UnknownMessageException(type);
			}
			// Construct Message object
			Message m = clazz.newInstance();
			
			// Deserialize message
			m.serialize(this);
			
			if(debug) {
				System.err.println("Received message: " + m.type);
			}
			
			return m;
		} catch (InstantiationException e) {
			// This should never happen. Ever.
			// If it does then there is something wrong with
			// a message class.
			finallyThrow = false;
			throw new Error(e);
		} catch (IllegalAccessException e) {
			// This should never happen. Ever.
			// Same as with InstantiationException above.
			finallyThrow = false;
			throw new Error(e);
		} catch (IOException e) {
			finallyThrow = false;
			throw e;
		} catch (UnknownMessageException e) {
			finallyThrow = false;
			throw e;
		} catch (Throwable th) {
			finallyThrow = false;
			throw th;
		} finally {
			// At the end of this block we should be at the beginning of the
			// next message in the stream regardless of if we received a message
			// with an unknown type or if there was an error in the
			// deserialization of the message.
			// So long as the length is correct... 
			try {
				// No matter what reset to the mark
				in.reset();
				// and skip to the end of the message
				in.skipBytes(len);
			} catch(Exception ex) {
				if(finallyThrow) {
					//Rethrow exception only nothing else was thrown above.
					throw ex;
				} else {
					System.err.println("Muted Exception:");
					ex.printStackTrace();
				}
			}
		}
	}
	
	
	// // Serializer API // //
	
	/**
	 * Deserialize a short
	 * @param s a short integer value
	 */
	public short serializeShort(short s) throws IOException {
		return in.readShort();
	}
	
	/**
	 * Deserialize an integer
	 * @param i an integer value
	 */
	public int serializeInt(int i) throws IOException {
		return in.readInt();
	}
	
	/**
	 * Deserialize a float
	 * @param f a floating point value.
	 */
	public float serializeFloat(float f) throws IOException {
		return in.readFloat();
	}
	
	/**
	 * Deserialize string
	 * @param s a string value 
	 */
	public String serializeString(String s) throws IOException {
		String string = new String(serializeByteArray(null), StandardCharsets.UTF_8);
		return string;
	}
	
	/**
	 * Deserialize byte array
	 * @param bytes a byte array
	 */
	public byte[] serializeByteArray(byte[] arr) throws IOException {
		int len = in.readInt();
		byte[] data;
		if(arr != null && arr.length == len) {
			data = arr;
		} else {
			data = new byte[len];
		}
		in.readFully(data);
		return data;
	}
	
}
