package protocol.messages;

import java.io.IOException;

/**
 * A Text message sent by a client and relayed by the server
 * to some remote client.
 * 
 * @author Daniel
 *
 */
public class TextMessage extends Message {

	// // Public API // //
	
	public final static short TYPE = 9;
	
	public int userid;
	public String message;
	
	public TextMessage() {
		super(TYPE);
	}
	
	public TextMessage(int userid, String message) {
		this();
		this.userid = userid;
		this.message = message;
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
		userid = s.serializeInt(userid);
		message = s.serializeString(message);
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, TextMessage.class);
	}
}