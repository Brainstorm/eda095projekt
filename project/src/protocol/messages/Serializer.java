package protocol.messages;

import java.io.IOException;

/**
 * <p>
 * Used by messages to pack or unpack itself to or from a stream. 
 * </p>
 * 
 * <b>Example:</b><br/>
 * To pack/unpack an int field called x do:
 * <pre>
 * x = serializeInt(x);
 * </pre>
 * 
 * @author Daniel
 *
 */
public interface Serializer {
	public short  serializeShort(short s) throws IOException;
	public int    serializeInt(int i) throws IOException;
	public float  serializeFloat(float f) throws IOException;
	public String serializeString(String s) throws IOException;
	
	// Note. This one is slightly different from the others in that it
	// checks the length of s when deserializing and may fill s with new data
	// if the length is the same as the array that was sent.
	// All others cannot modify their parameter as the parameter is immutable.
	// But this one might.
	public byte[] serializeByteArray(byte[] s) throws IOException;
}
