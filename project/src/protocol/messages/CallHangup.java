package protocol.messages;

import java.io.IOException;

/**
 * Sent by any client to server to disconnect the current call.
 * It is then relayed from server to the remote client.
 * 
 * It also used to disconnect a call that has not been accepted yet.
 * For example to refuse an incoming call or abort an accidental call to the
 * wrong end user, or about a call to an end user that is not picking up. 
 * 
 * The server should only relay this to the remote client after the IncomingCall
 * message has been sent to the remote client.
 * 
 * @author Daniel
 *
 */
public class CallHangup extends Message {

	// // Public API // //
	
	public final static short TYPE = 7;
	
	public CallHangup() {
		super(TYPE);
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, CallHangup.class);
	}
}
