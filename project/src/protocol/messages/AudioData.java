package protocol.messages;

import java.io.IOException;

/**
 * Sent by clients and relayed by server to remote client.
 * Sends audio data to the remote client.
 * @author Daniel
 *
 */
public class AudioData extends Message {

	// // Public API // //
	
	public final static short TYPE = 8;
	
	public byte[] data;
	
	public AudioData() {
		super(TYPE);
	}
	
	// Note: the data is not copied and may be modified.
	public AudioData(byte[] data) {
		super(TYPE);
		this.data = data;
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
		data = s.serializeByteArray(data);
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, AudioData.class);
	}
}