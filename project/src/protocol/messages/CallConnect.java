package protocol.messages;

import java.io.IOException;

import javax.sound.sampled.AudioFormat;

/**
 * Message sent from client to server to initiate a call to a user.
 * Note that the server sends a IncommingCallMessage type to the other user
 * and not a CallMessage.
 * @author Daniel
 *
 */
public class CallConnect extends Message {
	
	// // Public API // //
	
	public final static short TYPE = 1;
	
	public int userid;
	public AudioFormat format; 
	
	public CallConnect() {
		super(TYPE);
		// Dummy values.
		userid = -1;
		format = null;
	}
	
	public CallConnect(int userid, AudioFormat format) {
		this();
		this.userid = userid;
		this.format = format;
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
		userid = s.serializeInt(userid);
		format = SerializeAudioFormat.code(s, format);
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, CallConnect.class);
	}
}



