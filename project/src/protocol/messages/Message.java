package protocol.messages;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a message sent with this protocol.
 * All messages are reuseable i.e. you can send them twice,
 * then change something and send again.
 * All messages are basically simple data objects. That's why
 * I use public for a lot of the fields. They don't need
 * protection because there is no behavior in the message
 * classes that need there to be any correlation between
 * the fields.
 * 
 * @author Daniel
 *
 */

public abstract class Message {
	
	// // Public API // //
	
	// The Type id of the message
	// Used both for communication and when acting on a received message.
	public final short type;
	
	/**
	 * Serializes or deserializes data from or into the message using the
	 * Serializer.
	 * @param s A serialization thingy.
	 */
	public abstract void serialize(Serializer s)  throws IOException;
	
	/**
	 * Get the class of a message with a particular type.
	 * @param type the message type id
	 * @return the Class object of that message type
	 */
	public static Class<? extends Message> getMessageClass(short type) {
		return messageClasses.get(type);
	}
	
	// // Package Private SPI // //
	
	// Mapping from command to message class
	private final static Map<Short, Class<? extends Message>> messageClasses;
	
	// Static Initialization
	static {
		messageClasses = new HashMap<Short, Class<? extends Message>>();
	}
	
	/**
	 * Called from subclasses to register the class with a type id
	 * @param type the message type id
	 * @param clazz the message class object
	 */
	static void registerMessageClass(short type, Class<? extends Message> clazz) {
		if(messageClasses.containsKey(type)) {
			StringBuilder sb = new StringBuilder();
			sb.append("Message: Registration of class ");
			sb.append(clazz.getName());
			sb.append(" clashes with ");
			sb.append(messageClasses.get(type).getName());
			sb.append(" both claim type id ");
			sb.append(type);
			throw new AssertionError(sb.toString());
		}
		messageClasses.put(type, clazz);
	}
	
	// Constructor
	Message(short type) {
		this.type = type;
	}
	
}
