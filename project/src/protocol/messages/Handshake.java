package protocol.messages;

import java.io.IOException;

/**
 * The first message sent by both client and server.
 * If they see anything else as first message they receive then disconnect.
 * 
 * Also Servers may disconnect clients with weird names. Such as clients with
 * an empty name or a name with non-printable characters.
 * (Maybe we need an error message for that...)
 * 
 * @author Daniel
 *
 */
public class Handshake extends Message {

	// // Public API // //
	
	public final static short TYPE = 5;
	
	public int protocolVersion;
	public String displayName;
	
	public Handshake() {
		super(TYPE);
		protocolVersion = 0;
		displayName = "";
	}
	
	public Handshake(int protocolVersion, String displayName) {
		this();
		this.protocolVersion = protocolVersion;
		this.displayName = displayName;
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
		protocolVersion = s.serializeInt(protocolVersion);
		displayName = s.serializeString(displayName);
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, Handshake.class);
	}
}