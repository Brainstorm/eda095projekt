package protocol.messages;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioFormat.Encoding;

public class SerializeAudioFormat {
	
	private static final AudioFormat DUMMY_FORMAT = new AudioFormat(
			Encoding.PCM_SIGNED, 1,
			AudioSystem.NOT_SPECIFIED,
			AudioSystem.NOT_SPECIFIED,
			AudioSystem.NOT_SPECIFIED,
			1, false
		);

	public static AudioFormat code(Serializer s, AudioFormat f) throws IOException {
		if (f == null) {
			f = DUMMY_FORMAT;
		}
		String audioEncoding = encodingStrings.get(f.getEncoding());
		float sampleRate = f.getSampleRate();
		float frameRate = f.getFrameRate();
		int sampleSizeInBits = f.getSampleSizeInBits();
		int channels = f.getChannels();
		int frameSize = f.getFrameSize();
		boolean bigEndian = f.isBigEndian();
		
		audioEncoding = s.serializeString(audioEncoding);
		sampleRate = s.serializeFloat(sampleRate);
		frameRate = s.serializeFloat(frameRate);
		sampleSizeInBits = s.serializeInt(sampleSizeInBits);
		channels = s.serializeInt(channels);
		frameSize = s.serializeInt(frameSize);
		bigEndian = s.serializeInt(bigEndian?1:0) != 0;
		
		f = new AudioFormat(
				encodingKeys.get(audioEncoding),
				sampleRate,
				sampleSizeInBits,
				channels,
				frameSize,
				frameRate,
				bigEndian
			);
		
		return f;
	}
	
	static final Map<String, AudioFormat.Encoding> encodingKeys;
	static final Map<AudioFormat.Encoding, String> encodingStrings;
	
	static {
		encodingKeys = new HashMap<String, AudioFormat.Encoding>();
		encodingKeys.put("PCM_SIGNED", AudioFormat.Encoding.PCM_SIGNED);
		encodingKeys.put("PCM_UNSIGNED", AudioFormat.Encoding.PCM_UNSIGNED);
		encodingKeys.put("PCM_FLOAT", AudioFormat.Encoding.PCM_FLOAT);
		encodingKeys.put("ALAW", AudioFormat.Encoding.ALAW);
		encodingKeys.put("ULAW", AudioFormat.Encoding.ULAW);
		encodingStrings = new HashMap<AudioFormat.Encoding, String>();
		for(Entry<String, AudioFormat.Encoding> e : encodingKeys.entrySet()) {
			encodingStrings.put(e.getValue(), e.getKey());
		}
	}
}
