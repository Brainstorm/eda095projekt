package protocol.messages;

import java.io.IOException;

import javax.sound.sampled.AudioFormat;
/**
 * Sent from server to a client when another client wants to connect a call.
 * @author Daniel
 *
 */
public class IncomingCall extends Message {
	
	// // Public API // //
	
	public final static short TYPE = 2;
	
	// User id of the caller
	public int userid = -1;
	// Display name of the caller
	public String username = "";
	// Audio format from the caller
	public AudioFormat format = null;
	
	public IncomingCall() {
		super(TYPE);
	}
	
	public IncomingCall(int userid, String username, AudioFormat format) {
		this();
		this.userid = userid;
		this.username = username;
		this.format = format;
	}
	

	// // Message Serialization SPI // //
	@Override
	public void serialize(Serializer s) throws IOException {
		userid = s.serializeInt(userid);
		username = s.serializeString(username);
		format = SerializeAudioFormat.code(s, format);
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, IncomingCall.class);
	}
}
