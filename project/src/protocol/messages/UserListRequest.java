package protocol.messages;

import java.io.IOException;

/**
 * Sent from client to server when the user wants to see the
 * list of connected users.
 * @author Daniel
 *
 */
public class UserListRequest extends Message {

	// // Public API // //
	
	public final static short TYPE = 3;
	
	public UserListRequest() {
		super(TYPE);
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, UserListRequest.class);
	}
}
