package protocol.messages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Sent from server to client to tell it what users are online.
 * Could be at any time (after Handshake) but usually in response
 * to a ListUsersMessage.
 * @author Daniel
 *
 */
public class UserListReply extends Message {

	// // Public API // //
	
	public final static short TYPE = 4;
	
	public ArrayList<User> users;
	
	public UserListReply() {
		super(TYPE);
		users = new ArrayList<User>();
	}
	
	public UserListReply(List<User> users) {
		super(TYPE);
		this.users = new ArrayList<User>(users);
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
		
		// Sync list size
		int size = s.serializeInt(users.size());
		
		// There is no 'good' way of doing this in java.
		// In c++ this would just be vector.resize(size);
		if(size <= users.size()) {
			users.subList(size, users.size()).clear();			
		} else {
			users.ensureCapacity(size);
			for (int i = users.size(); i < size; i++) {
				users.add(new User());
			}
		}
		
		for (User user: users) {
			user.userid = s.serializeInt(user.userid);
			user.username = s.serializeString(user.username);
		}
		
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, UserListReply.class);
	}
}
