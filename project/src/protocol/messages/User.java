package protocol.messages;

public class User {
	public int userid;
	public String username;

	public User(int userid, String username) {
		this.userid = userid;
		this.username = username;
	}

	public User() {
		this(-1, "");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + userid;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userid != other.userid)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return username;
	}
}