package protocol.messages;

import java.io.IOException;

/**
 * Sent from a client to the server to pickup the call.
 * Is discarded if the server has not sent an IncommingCall to the client or
 * if the remote client has already disconnected the call.
 * 
 * This message is relayed to the remote client by the Server.
 * 
 * After this message is received by the server the call is connected and
 * the client who picked up can send audio data.
 * After this message has been received by the remote client the it
 * knows the call is connected and can send audio data.
 * 
 * @author Daniel
 *
 */
public class CallPickup extends Message {

	// // Public API // //

	public final static short TYPE = 6;

	//TODO add audio format information
	
	public CallPickup() {
		super(TYPE);
	}
	
	// // Message Serialization SPI // //

	@Override
	public void serialize(Serializer s) throws IOException {
	}
	
	// Register with message SPI
	static {
		registerMessageClass(TYPE, CallPickup.class);
	}
}

