package protocol;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import protocol.messages.Message;
import protocol.messages.Serializer;

/**
 * <p>
 * 	Deals with sending message on a connection.
 * </p>
 * 
 * <p>
 *  Note that all messages are serialized into a buffer first before sent.
 *  This is to be able to calculate the length of the message on the wire
 *  and send that in the beginning of the message.
 * </p>
 * 
 * @author Daniel
 *
 */

class StreamSender implements ChatSender, Serializer {
	
	private boolean debug = true;
	
	private DataOutputStream out;
	private ByteArrayOutputStream buffer;
	private DataOutputStream dataBuffer;
	
	public StreamSender(OutputStream out) {
		this.out = new DataOutputStream(new BufferedOutputStream(out));
		buffer = new ByteArrayOutputStream();
		dataBuffer = new DataOutputStream(buffer);
	}

	// // Public API // // 
	
	/* (non-Javadoc)
	 * @see protocol.ChatSender#sendMessage(protocol.Message)
	 */
	@Override
	public void sendMessage(Message m) throws IOException {
		messageBegin();
		m.serialize(this);
		messageEnd(m.type);
		
		if(debug) {
			System.err.println("Sent message: " + m.type);
		}
		
	}
	
	// // Package Private SPI // //
	
	/**
	 * Serialize a short
	 * @param s a short integer value
	 */
	public short serializeShort(short s) throws IOException {
		dataBuffer.writeShort(s);
		return s;
	}
	
	/**
	 * Serialize an integer
	 * @param i an integer value
	 */
	public int serializeInt(int i) throws IOException {
		dataBuffer.writeInt(i);
		return i;
	}
	
	/**
	 * Serialize a float
	 * @param f a floating point value.
	 */
	public float serializeFloat(float f) throws IOException {
		dataBuffer.writeFloat(f);
		return f;
	}
	
	/**
	 * Serialize string
	 * @param s a string value 
	 */
	public String serializeString(String s) throws IOException {
		byte[] data = s.getBytes(StandardCharsets.UTF_8);
		serializeByteArray(data);
		return s;
	}
	
	/**
	 * Serialize byte array
	 * @param bytes a byte array
	 */
	public byte[] serializeByteArray(byte[] bytes) throws IOException {
		dataBuffer.writeInt(bytes.length);
		dataBuffer.write(bytes);
		return bytes;
	}
	
	// // Private Methods // //
	
	private void messageBegin() throws IOException {
		buffer.reset();
	}
	
	private void messageEnd(short typeid) throws IOException {
		// Send typeid
		out.writeShort(typeid);
		// Put in the length of the message
		out.writeInt(buffer.size());
		// Write the message to out.
		buffer.writeTo(out);
		// Get it all sent as soon as possible.
		out.flush();
	}
	
}
