package server;

/*
 Starts the server and handles new connections.
 */
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import protocol.ChatConnection;

public class Server extends Thread {

	private ServerSocket ss;
	private Monitor monitor;

	public Server(int port) {
		monitor = new Monitor();
		try {
			ss = new ServerSocket(port);
		} catch (IOException e) {
			System.out.print("Port is unavailable! Try another one.");
			System.exit(0);
		}

	}

	public static void main(String args[]) {
		if (args.length > 0) {
			new Server(Integer.parseInt(args[0])).start();
		} else {
			new Server(3000).start();
		}
	}

	public void run() {
		Socket cs;
		ConnectionState state;
		while (true) {
			try {
				cs = ss.accept();
				state = new ConnectionState(monitor);
				ChatConnection connection = new ChatConnection(cs.getInputStream(), cs.getOutputStream());
				new ClientSender(connection.getSender(), state).start();
				new ClientReceiver(connection.getReceiver(), state).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
