package server;

import java.util.ArrayList;
import java.util.HashMap;

import protocol.messages.User;

/*
 Should contain the server state. e.g. who is connected to the server.
 
 
 
 */

public class Monitor {

	HashMap<Integer, ConnectionState> connections = new HashMap<Integer, ConnectionState>();
	HashMap<Integer, User> users= new HashMap<Integer, User>();

	synchronized public void AddConnection(ConnectionState state) {
		User user=state.getUser();
		connections.put(user.userid, state);
		users.put(user.userid, user);
		updateUserList();
	}


	synchronized public void removeConnection(ConnectionState state) {
		int userid=state.getUser().userid;
		connections.remove(userid);
		users.remove(userid);
		updateUserList();
	}

	synchronized public ArrayList<User> getAllConnections() {
		return new ArrayList<User>(users.values());
	}

	synchronized public ConnectionState getConnection(int userid) {
		return connections.get(userid);
	}
	
	private synchronized void updateUserList() {
		for(ConnectionState conn: connections.values()) {
			conn.sendUserList();
		}
	}
}
