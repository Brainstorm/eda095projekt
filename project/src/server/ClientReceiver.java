package server;

/*
 Recives packages from the connected client and takes appropriate action.
 */

import java.io.EOFException;
import java.io.IOException;

import protocol.ChatReceiver;
import protocol.UnknownMessageException;

public class ClientReceiver extends Thread {
    private ChatReceiver receiver;
    private ConnectionState state;
    private boolean connected = true;

    public ClientReceiver(ChatReceiver receiver, ConnectionState state) {
        this.receiver = receiver;
        this.state = state;
    }

    public void run() {
        while (connected) {
            try {
                state.registerMessage(receiver.receive());
                
            } catch (EOFException e) {
                state.logout();
                connected = false;
			} catch (IOException e) {
				continue;
            } catch (UnknownMessageException e) {
            	continue;
            }
        }
    }
}
