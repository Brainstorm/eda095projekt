package server;

/*
 Handles all outgoing traffic to a single client.
 */
import java.io.IOException;

import protocol.ChatSender;

public class ClientSender extends Thread {
	private ChatSender sender;
	private ConnectionState state;
	private boolean connected = true;

	public ClientSender(ChatSender sender, ConnectionState state) {
		this.sender = sender;
		this.state = state;
	}

	public void run() {
		while (connected) {
			try {
				sender.sendMessage(state.getNextMessage());
			} catch (IOException e) {
			} catch (InterruptedException e) {
				connected=false;
			}
		}
	}
}
