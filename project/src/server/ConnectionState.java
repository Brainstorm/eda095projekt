package server;

import java.util.LinkedList;

import javax.sound.sampled.AudioFormat;

import protocol.ChatProtocol;
import protocol.messages.AudioData;
import protocol.messages.CallConnect;
import protocol.messages.CallHangup;
import protocol.messages.CallPickup;
import protocol.messages.Handshake;
import protocol.messages.IncomingCall;
import protocol.messages.Message;
import protocol.messages.TextMessage;
import protocol.messages.User;
import protocol.messages.UserListReply;
import protocol.messages.UserListRequest;

/**
 * Contains the state for a connection and handles all actions associated with a
 * message
 */
public class ConnectionState {
    private static int currentUserID = 1;
    private User user;
    private LinkedList<Message> outgoingMessages = new LinkedList<Message>();
    private Monitor monitor;
    private boolean inCall = false;
    private ConnectionState otherEnd;
    private boolean isConnected=true;

    public ConnectionState(Monitor monitor) {
        this.monitor = monitor;
        sendMessage(new Handshake(ChatProtocol.VERSION, "Server"));
    }

    synchronized public void registerMessage(Message message) {
        switch (message.type) {
            case AudioData.TYPE:
                if (inCall && otherEnd != null) otherEnd.putAudio(user, (AudioData) message);
                return;
            case CallConnect.TYPE:
                callConnect((CallConnect) message);
                return;
            case CallHangup.TYPE:
                otherEnd.hangup(user);
                hangup();
                return;
            case CallPickup.TYPE:
                pickupCall((CallPickup) message);
                return;
            case Handshake.TYPE:
                doHandshake((Handshake) message);
                return;
            case UserListRequest.TYPE:
            	sendUserList();
                return;
            case TextMessage.TYPE:
            	relayTextMessage((TextMessage) message);
            default:
                //TODO Should not happen! Do something smart, maybe add some error handling. Log it!!!
        }
    }
    
    private void relayTextMessage(TextMessage message) {
		ConnectionState conn = monitor.getConnection(message.userid);
		message.userid = user.userid;
		conn.sendMessage(message);
	}

	synchronized private void sendMessage(Message m) {
    	outgoingMessages.add(m);
    	notifyAll();
    }
    
	synchronized public void sendUserList() {
		sendMessage(new UserListReply(monitor.getAllConnections()));
	}

    synchronized private void putAudio(User user, AudioData message) {
        if (user.equals(otherEnd.getUser())) {//Dont accept audio from someone not in the call.
            sendMessage(message);
        }
    }

    synchronized private void doHandshake(Handshake message) {
            user = new User(currentUserID++, message.displayName);
            monitor.AddConnection(this);
    }

    synchronized private void pickupCall(CallPickup message) {
    	if(otherEnd != null) {
    		inCall = true;
    		otherEnd.startCall(user);
    	} // else ?? // Eh?
    }

    synchronized private void startCall(User user) {
    	assert(otherEnd.getUser().equals(user));
    	inCall = true;
    	sendMessage(new CallPickup());
    	
    }

    synchronized private void callConnect(CallConnect message) {
        if (otherEnd == null && !inCall) { //Cannot open a new call while being in a call.
            int userid = message.userid;
            otherEnd = monitor.getConnection(userid);
            if(otherEnd == null) {
            	sendMessage(new CallHangup());
            }
            if(!otherEnd.callConnect(this, user, message.format)) {
            	// Other User already in call
            	sendMessage(new CallHangup());
            	hangup();
            }
        }
    }

    synchronized private void hangup(User user) {
    	assert(otherEnd.getUser().equals(user));
        hangup();
        sendMessage(new CallHangup());
    }

    synchronized private void hangup() {
        inCall = false;
        otherEnd = null; // Just to allow the GC to clean it up if the user signs out.
    }

    synchronized private boolean callConnect(ConnectionState otherEnd, User user, AudioFormat format) {
    	if(this.otherEnd != null || inCall) {
    		return false;
    	}
    	
    	this.otherEnd = otherEnd;
        sendMessage(new IncomingCall(user.userid, user.username, format));
        
        return true;
    }

    synchronized public Message getNextMessage() throws InterruptedException {
        while (outgoingMessages.size() < 1 && isConnected) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        if(!isConnected) throw new InterruptedException();
        return outgoingMessages.pop();
    }

    synchronized public void logout() {
        monitor.removeConnection(this);
        if (otherEnd != null) otherEnd.sendMessage(new CallHangup());
        isConnected=false;
        notifyAll();
    }

    synchronized public User getUser() {
        return user;
    }
}
