package client;

import client.controllers.*;
import client.gui.MainFrame;

/**
 * This is the main class to be loaded on start up.
 * Creates a client, main frame and their controllers.
 * @author Gustaf
 * 
 */
public class Main {
	
	public static void main(String[] args) {
		Client c = new Client();
		MainFrame f = new MainFrame();
		c.addObserver(new CallController(c, f.getCallPanel()));
		c.addObserver(new ConnectController(c, f.getConnectPanel()));
		c.addObserver(new ChatController(c, f));
	}
	
}
