package client.gui;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import client.Client;
import client.gui.components.ActionButton;
import client.gui.components.HeaderLabel;
import client.gui.components.ScrollableUserList;
import protocol.messages.User;

/**
 * Panel containing GUI components related to the calling of
 * other users. The user can select one of the available users
 * and ask the client to call them.
 * @author Gustaf
 * 
 */
public class CallPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private ScrollableUserList userList;
	private ActionButton callButton;
	private ActionButton chatButton;
	private HeaderLabel usersOnline;
	
	public CallPanel () {
		setLayout(null);
		
		usersOnline = new HeaderLabel("Users Online", 10);
		userList = new ScrollableUserList(200, 150, 30);
		callButton = new ActionButton("Connect", 185);
		chatButton = new ActionButton("Chat", 215);
		chatButton.setVisible(false);
		
		add(usersOnline);
		add(userList);
		add(callButton);
		add(chatButton);
	}
	
	public void addChatPanelListener(ActionListener listener) {
		chatButton.addActionListener(listener);
	}
	
	public void addCallPanelListener(ActionListener listener) {
		callButton.addActionListener(listener);
	}
	
	public void updateUserList(ArrayList<User> newList) {
		userList.update(newList);
	}
	
	public User getSelectedUser() {
		return userList.getSelected();
	}
	
	public void updateCallButton(int i) {
		if (i == Client.ACTIVECALL) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					callButton.changeFunction("End");
				}
			});
		} else if (i == Client.CONNECTED) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					callButton.changeFunction("Call");
					chatButton.setVisible(true);
				}
			});
		} else {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					chatButton.setVisible(false);
				}
			});
		}
	}
}
