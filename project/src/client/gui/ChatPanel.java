package client.gui;

import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class ChatPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton sendButton;
	private JTextArea messagesArea;
	private JTextArea inputArea;
	private JScrollPane messagesPane;
	private boolean isOffline = false;
	
	public ChatPanel() {
		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		
		sendButton = new JButton("Send");
		messagesArea = new JTextArea();
		messagesPane = new JScrollPane(messagesArea);
		inputArea = new JTextArea();
		
		messagesArea.setEditable(false);
		
		add(messagesPane);
		add(inputArea);
		add(sendButton);
		
		layout.setVerticalGroup(
			layout.createSequentialGroup()
				.addComponent(messagesPane, 200, 200, 20000)
				.addGroup(layout.createParallelGroup()
					.addComponent(inputArea, 30, 50, 200)
					.addComponent(sendButton, 30, 50, 200)
				)
		);
		layout.setHorizontalGroup(
			layout.createParallelGroup()
				.addComponent(messagesPane, 200, 300, 20000)
				.addGroup(layout.createSequentialGroup()
					.addComponent(inputArea, 150, 220, 20000)
					.addComponent(sendButton, 50, 80, 100)
				)
		);
		
	}
	
	public void addActionListener(ActionListener a) {
		sendButton.addActionListener(a);
	}
	
	public void append(final String message) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				messagesArea.append(message + "\n");
			}
		});
	}
	
	public void setOffline() {
		if(!this.isOffline) {
			this.isOffline = true;
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					messagesArea.append("\nUser disconnected.\n");
				}
			});
		}
	}

	public String getMessage() {
		String message = inputArea.getText(); 
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				inputArea.setText("");
			}
		});
		return message;
	}
	
	
	
	
}
