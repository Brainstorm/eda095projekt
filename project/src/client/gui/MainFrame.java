package client.gui;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

/**
 * Represents the window that the user sees and
 * contains panels for holding GUI components.
 * @author Gustaf
 * 
 */
public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 300;
	public static final int HEIGHT = 310;
	private JTabbedPane tabs;
	private ConnectPanel connectPanel;
	private CallPanel callPanel;
	
	public MainFrame()  {
		//Frame properties
		setTitle("QuickVoIP 1.0");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(MainFrame.WIDTH, MainFrame.HEIGHT));
		setMaximumSize(new Dimension(MainFrame.WIDTH, MainFrame.HEIGHT));
		setPreferredSize(new Dimension(MainFrame.WIDTH, MainFrame.HEIGHT));
		
		//Create panels and add them to them to the frame
		tabs = new JTabbedPane();
		connectPanel = new ConnectPanel();
		callPanel = new CallPanel();
		tabs.addTab("Connect", connectPanel);
		tabs.addTab("Users", callPanel);
		
		//Add Panels and make the frame visible
		getContentPane().add(tabs);
		pack();
		setVisible(true);
	}	
	
	public ConnectPanel getConnectPanel() {
		return connectPanel;
	}
	
	public CallPanel getCallPanel() {
		return callPanel;
	}

	public void addTab(final String title, final JPanel p) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				tabs.addTab(title, p);
			}
		});
	}
}