package client.gui.components;

import java.awt.Dimension;
import javax.swing.JButton;
import client.gui.MainFrame;

public class ActionButton extends JButton {
	private static final long serialVersionUID = 1L;
	private Dimension dim;
	private int yAxis;
	
	public ActionButton(String function, int yAxis) {
		this.yAxis = yAxis;
		changeFunction(function);
	}
	
	public void changeFunction(String function) {
		setText(function);
		setActionCommand(function);
		adjustSize();
	}
	
	private void adjustSize() {
		dim = getPreferredSize();
		setBounds((MainFrame.WIDTH / 2) - (dim.width / 2), yAxis, dim.width, dim.height);
	}
}
