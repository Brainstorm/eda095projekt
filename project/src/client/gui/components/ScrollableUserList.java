package client.gui.components;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import client.gui.MainFrame;
import protocol.messages.User;

/**
 * Represents a scrollable list used for
 * showing what users are online.
 * @author Gustaf
 * 
 */
public class ScrollableUserList extends JScrollPane {
	private static final long serialVersionUID = 1L;
	private DefaultListModel<User> model;
	private JList<User> userList;
	private int width, height, yAxis;

	public ScrollableUserList(int width, int height, int yAxis) {
		this.width = width;
		this.height = height;
		this.yAxis = yAxis;
		model = new DefaultListModel<User>();
		userList = new JList<User>();
		userList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		setViewportView(userList);
		adjustSize();
	}

	public User getSelected() {
		return userList.getSelectedValue();
	}
	
	private void adjustSize() {
		setBounds((MainFrame.WIDTH / 2) - (width / 2), yAxis, width, height);
	}
	
	public void update(ArrayList<User> friendsOnline) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				model.clear();
				for (User u : friendsOnline)
					model.addElement(u);
				userList.setModel(model);
			}
		});
	}
}