package client.gui.components;

import javax.swing.JTextField;

import client.gui.MainFrame;

public class InputField extends JTextField {
	private static final long serialVersionUID = 1L;
	private int width, height, yAxis;
	
	public InputField(int width, int height, int yAxis) {
		this.width = width;
		this.height = height;
		this.yAxis = yAxis;
		adjustSize();
	}
	
	private void adjustSize() {
		setBounds((MainFrame.WIDTH / 2) - (width / 2), yAxis, width, height);
	}
}
