package client.gui.components;

import java.awt.Dimension;
import javax.swing.JLabel;
import client.gui.MainFrame;

public class HeaderLabel extends JLabel {
	private static final long serialVersionUID = 1L;
	private Dimension dim;
	private int yAxis;
	
	public HeaderLabel (String text, int yAxis) {
		super(text);
		this.yAxis = yAxis;
		adjustSize();
	}
	
	private void adjustSize() {
		dim = getPreferredSize();
		setBounds((MainFrame.WIDTH / 2) - (dim.width / 2), yAxis, dim.width, dim.height);
	}
}
