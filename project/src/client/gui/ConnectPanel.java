package client.gui;

import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import client.Client;
import client.gui.components.ActionButton;
import client.gui.components.HeaderLabel;
import client.gui.components.InputField;

/**
 * Panel containing GUI components used for input of connection
 * details and connecting to servers. The user can fill in connection details and
 * ask to connect or disconnect from a server.
 * @author Gustaf
 * 
 */
public class ConnectPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private InputField serverAddress, displayName;
	private ActionButton connectButton;
	private HeaderLabel hostLabel, nameLabel;

	public ConnectPanel() {
		setLayout(null);
		
		hostLabel = new HeaderLabel("Host Address", 10);
		serverAddress = new InputField(200, 25, 30);
		nameLabel = new HeaderLabel("Display Name", 60);
		displayName = new InputField(200, 25, 80);
		connectButton = new ActionButton("Connect", 120);

		add(hostLabel);
		add(serverAddress);
		add(nameLabel);
		add(displayName);
		add(connectButton);
	}

	public void addConnectPanelListener(ActionListener listener) {
		connectButton.addActionListener(listener);
	}

	public String getAddress() {
		return serverAddress.getText();
	}

	public String getDisplayName() {
		return displayName.getText();
	}
	
	public void updateConnectButton(int i) {
		if (i == Client.DOWN) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					connectButton.changeFunction("Connect");
				}
			});
		} else if (i == Client.CONNECTED) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					connectButton.changeFunction("Disconnect");
				}
			});
		}
	}
}
