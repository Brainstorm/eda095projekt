package client.sound;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

public class Audio {
	
	byte[] temp= new byte[]{};
	private int counter=0;
	
	// // Recording functions // //
	
	public boolean startRecording() {
		if(inputLine != null) {
			return true;
		}
		
		try {
			inputLine = AudioSystem.getTargetDataLine(recordingFormat);
			inputLine.open(recordingFormat, internalRecoringBufferSize);
			inputLine.start();
			return true;
		} catch (IllegalArgumentException e) {
			inputLine = null;
			return false;
		} catch (LineUnavailableException e) {
			inputLine = null;
			return false;
		}
	}
	
	public void stopRecording() {
		if (inputLine == null) {
			return;
		}
		
		inputLine.stop();
		inputLine.close();
		inputLine = null;
	}
	
	public void record(byte[] buffer) {
		if (inputLine == null) {
			throw new IllegalStateException("Recording is not started.");
		}
		int read = inputLine.read(buffer, 0, buffer.length);
		for(int i = read; i < buffer.length; i++) {
			buffer[i] = 0;
		}
	}
	
	public static AudioFormat getRecordingFormat() {
		return recordingFormat;
	}
	
	// // Playback functions // //
	
	public boolean canPlayFormat(AudioFormat af) {
		// A bit hackish... There is room for improvement here.
		try {
			AudioSystem.getSourceDataLine(af);
			return true;
		} catch (LineUnavailableException e) {
			return false;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	public boolean startPlaying(AudioFormat af) {
		if(outputLine != null) {
			return true;
		}
		
		try {
			outputLine = AudioSystem.getSourceDataLine(af);
			outputLine.open(af, 44100);
			outputLine.start();
			return true;
		} catch (IllegalArgumentException e) {
			//Shouldn't need to catch this if canPlayFormat is called first.
			throw new RuntimeException(e);
		} catch (LineUnavailableException e) {
			outputLine.close();
			outputLine = null;
			return false;
		}
	}
	
	/**
	 * Note this one is blocking until the audio data has been drained.
	 */
	public void stopPlaying() {
		if (outputLine == null) {
			return;
		}
		outputLine.stop();
		outputLine.drain();
		outputLine.close();
		outputLine = null;
	}
	
	public void play(byte[] buffer) {
		if (outputLine == null) {
			throw new IllegalStateException("Playing is not started.");
		}else if(counter<3){
			counter++;
			byte[] temp = new byte[this.temp.length + buffer.length];
			System.arraycopy(this.temp, 0, temp, 0, this.temp.length);
			System.arraycopy(buffer, 0, temp, this.temp.length, buffer.length);
			this.temp=temp;		
		}else{
			outputLine.write(temp, 0, temp.length);
			temp= new byte[]{};		
		}
		
		outputLine.write(buffer, 0, buffer.length);
	}
	
	// // Private stuffs // //
	
	//Recording parameters:
	private final static float sampleRate = 44100f;
	private final static float frameRate = 44100f;
	private final static int sampleSizeInBits = 16;
	private final static int channels = 2;
	private final static int frameSize = 4;
	private final static boolean bigEndian = false;
	
	// Have a 1 second recording buffer
	private final static int internalRecoringBufferSize = 44100 * frameSize;
	
	private final static AudioFormat recordingFormat = new AudioFormat(
			AudioFormat.Encoding.PCM_SIGNED,
			sampleRate, sampleSizeInBits, channels,
			frameSize, frameRate, bigEndian
		);
	
	// Recording line.
	private TargetDataLine inputLine;
	
	// Output line.
	private SourceDataLine outputLine;
	
}
