package client.sound;

import java.io.IOException;
import java.util.Scanner;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;


public class AudioTest {
	
	public static void main(String[] args) throws LineUnavailableException, IOException {
		Audio a = new Audio();
		
		if(!a.canPlayFormat(Audio.getRecordingFormat())) {
			System.out.println("Cannot play.");
			return;
		}
		
		if(!a.startRecording()) {
			System.err.println("Could not start recording.");
			return;
		}
		
		if(!a.startPlaying(Audio.getRecordingFormat())) {
			System.err.println("Could not start playing.");
			return;
		}
		
		byte[] buffer = new byte[4000];
		for(long i=0; i<44100L*4L*10L; i+=buffer.length) {
			a.record(buffer);
			a.play(buffer);
		}
		
		a.startRecording();
		a.stopPlaying();
	}

	private static void simple() throws LineUnavailableException {
		// Choose a common audio format
		// This is CD-quality stereo audio
		AudioFormat af = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100f, 16, 2, 4, 44100f, false);
		
		// Setup sound input (confusingly called a target line)
		TargetDataLine inputLine;
		inputLine = AudioSystem.getTargetDataLine(af);
		
		// Setup sound output (even more confusingly called a source line)
		SourceDataLine outputLine = AudioSystem.getSourceDataLine(af);
		
		// Open the lines 
		inputLine.open(af, 44100);
		outputLine.open(af, 44100);
		
		inputLine.start();
		outputLine.start();
		
		displayWarning();
		
		// Copy over
		byte[] buffer = new byte[20];
		while(true) {
			int bytesRead = inputLine.read(buffer, 0, buffer.length);
			//System.out.println("Read: " + bytesRead);
			outputLine.write(buffer, 0, bytesRead);
		}
	}

	private static void displayWarning() {
		Scanner scan = new Scanner(System.in);
		System.out.println("\n\tWARNING!");
		System.out.println("Without headphones you'll get a feedback-loop!!! also check your volume...\n");
		System.out.println("Are you using headphones? (yes/no)");
		if(!scan.nextLine().trim().equalsIgnoreCase("yes")) {
			System.exit(0);
		}
		scan.close();
	}
	
	
	
}
