package client;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import client.sound.Audio;
import protocol.ChatReceiver;
import protocol.UnknownMessageException;
import protocol.messages.AudioData;
import protocol.messages.CallHangup;
import protocol.messages.CallPickup;
import protocol.messages.Handshake;
import protocol.messages.IncomingCall;
import protocol.messages.Message;
import protocol.messages.TextMessage;
import protocol.messages.User;
import protocol.messages.UserListReply;

/**
 * Handles incoming traffic from the server and updates the
 * client with the new data. Prompts the user for input
 * when needed. 
 * @author Gustaf
 * 
 */
public class ClientReceiver extends Thread {
	private ChatReceiver receiver;
	private Client client;
	private Audio audio;
	
	public ClientReceiver(Client client, ChatReceiver receiver, Audio audio) {
		this.audio = audio;
		this.client = client;
		this.receiver = receiver;
	}

	public void run() {
		Message message = null;
		while (true) {
			try {
				message = receiver.receive();
			} catch (EOFException e) {
				break;
			} catch (IOException e) {
				e.printStackTrace();
				break;
			} catch (UnknownMessageException e) {
				e.printStackTrace();
				continue;
			}
			switch (message.type) {
			case Handshake.TYPE:
				client.setStatus(Client.CONNECTED);
				break;
			case IncomingCall.TYPE:
				IncomingCall callMessage = (IncomingCall) message;
				final String caller = callMessage.username + "(" + callMessage.userid + ")";
				final Object[] options = { "Reject", "Answer" };
				try {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						int option = JOptionPane.showOptionDialog(null, caller , "Incoming call",
									 JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, 
									 null, options, options[0]);
						if (option == 1 && client.getStatus() != Client.ACTIVECALL) {
							client.setStatus(Client.ACTIVECALL);
							client.addMessage(new CallPickup());
						} else {
							client.addMessage(new CallHangup());
						}
					}
				});
				} catch (Exception e) {	
					e.printStackTrace();
				}
				break;
			case CallPickup.TYPE:
				client.setStatus(Client.ACTIVECALL);
				break;
			case CallHangup.TYPE:
				client.setStatus(Client.CONNECTED);
				break;
			case UserListReply.TYPE:
				UserListReply userListMessage = (UserListReply) message;
				ArrayList<User> temp = userListMessage.users;
				client.updateCurrentUsers(temp);
				break;
			case AudioData.TYPE:
				AudioData ad = (AudioData) message;
				audio.play(ad.data);				
				break;
			case TextMessage.TYPE:
				TextMessage text = (TextMessage) message;
				client.addChatMessage(text);
				break;
			default:
				System.out.println("Uknown message of type: " + message.type);
			}
		}
	}
}
