package client;

import protocol.messages.AudioData;
import client.sound.Audio;

public class AudioRecorder extends Thread {
	private byte[] buffer;
	private Audio audio;
	private Client client;
	
	public AudioRecorder (Client client, Audio audio) {
		buffer = new byte[1000];
		this.audio = audio;
		this.client = client;
	}
	
	public void run() {
		while (client.getStatus() == Client.ACTIVECALL) {
			audio.record(buffer);
			client.addMessage(new AudioData(buffer));
		}
	}
}
