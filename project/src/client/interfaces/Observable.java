package client.interfaces;

/**
 * Interface for classes that need to notify others of changes.
 * @author Gustaf
 * 
 */
public interface Observable {
	
	public void addObserver(Observer o);
	
	public void removeObserver(Observer o);
	
	public void notifyObservers();
	
}
