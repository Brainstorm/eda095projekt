package client.interfaces;

/**
 * Interfaces for classes that need to listen for changes.
 * @author Gustaf
 * 
 */
public interface Observer {
	
	public void update();
	
}
