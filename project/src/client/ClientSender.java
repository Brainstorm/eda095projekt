package client;

import java.io.IOException;
import protocol.ChatSender;

/**
 * Fetches messages from the client and sends them
 * to the server. Sets the client status to down it fails.
 * @author Gustaf
 * 
 */
public class ClientSender extends Thread {
	private ChatSender sender = null;
	private Client client;
	
	public ClientSender(Client client, ChatSender sender) {
		this.client = client;
		this.sender = sender;
	}
	
	public void run() {
		while (true) {
			try {
				sender.sendMessage(client.getNextMessage());
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}
}
