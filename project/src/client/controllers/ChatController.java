package client.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import protocol.messages.TextMessage;
import protocol.messages.User;
import client.Client;
import client.gui.ChatPanel;
import client.gui.MainFrame;
import client.interfaces.Observer;

public class ChatController implements Observer, ActionListener {
	Map<Integer, ChatPanel> panels;
	Map<Integer, User> users;
	MainFrame frame;
	Client client;
	
	public ChatController(Client client, MainFrame frame) {
		super();
		this.client = client;
		this.frame = frame;
		
		panels = new HashMap<>();
		users = new HashMap<>();
		
		frame.getCallPanel().addChatPanelListener(this);
	}

	@Override
	public void update() {
		// Check with user list if any client has left.
		ArrayList<User> usersList = client.getCurrentUsers();
		HashMap<Integer, User> newUsers = new HashMap<>();
		if(usersList != null) {
			for(User user: usersList) {
				newUsers.put(user.userid, user);
			}
		}
		
		for(Integer userid: panels.keySet()) {
			if(!newUsers.containsKey(userid)) {
				panels.get(userid).setOffline();
			}
		}
		
		users.putAll(newUsers);
		
		// Check if there are new messages
		final ArrayList<TextMessage> messages = client.getNewChatMessages();
		for(TextMessage text: messages) {
			ChatPanel p = panels.get(text.userid);
			User user = users.get(text.userid);
			if(p == null) {
				p = startChat(user);
			}
			
			p.append(user.username + ": " + text.message);
		}
		
	}
	
	public ChatPanel startChat(User user) {
		if(user == null) {
			throw new IllegalArgumentException("User cannot be null");
		}
		
		if(panels.containsKey(user.userid)) {
			return panels.get(user.userid);
		}
		
		String name = user.username;
		
		ChatPanel p = new ChatPanel();
		p.addActionListener(new Listener(p, user.userid));
		frame.addTab("Chat: " + name, p);
		
		panels.put(user.userid, p);
		
		return p;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equalsIgnoreCase("Chat")) {
			User user = frame.getCallPanel().getSelectedUser();
			startChat(user);
		}
	}
	
	class Listener implements ActionListener {
		ChatPanel p;
		int userid;
		
		public Listener(ChatPanel p, int userid) {
			this.p = p;
			this.userid = userid;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String text = p.getMessage();
			p.append("You: " + text);
			client.addMessage(new TextMessage(userid, text));
		}		
	}
	
	
}
