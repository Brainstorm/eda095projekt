package client.controllers;

import client.interfaces.Observer;
import client.sound.Audio;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import protocol.messages.CallConnect;
import protocol.messages.CallHangup;
import protocol.messages.User;
import client.gui.CallPanel;
import client.Client;

/**
 * Listens to changes in the call panel and client
 * and updates both accordingly depending on the source.
 * @author Gustaf
 * 
 */
public class CallController implements Observer {
	private Client client;
	private CallPanel panel;

	public CallController(Client c, CallPanel p) {
		client = c;
		panel = p;
		panel.addCallPanelListener(new CallPanelListener());
	}

	private class CallPanelListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Call") && client.getStatus() == Client.CONNECTED) {
				User selectedUser = panel.getSelectedUser();
				if (selectedUser != null) {
					client.addMessage(new CallConnect(selectedUser.userid, Audio.getRecordingFormat()));
				}
			} else if (e.getActionCommand().equals("End")) {
				if (client.getStatus() == Client.ACTIVECALL) {
					client.setStatus(Client.CONNECTED);
					client.addMessage(new CallHangup());
				}
			}
		}
	}

	@Override
	public void update() {
		ArrayList<User> currentList = client.getCurrentUsers();
		if (currentList != null) {
			panel.updateUserList(currentList);
		}
		int status = client.getStatus();
		panel.updateCallButton(status);
	}
}
