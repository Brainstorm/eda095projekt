package client.controllers;

import client.interfaces.Observer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import client.gui.ConnectPanel;
import client.Client;

/**
 * Listens to changes in the connect panel and client
 * and updates them accordingly depending on the source.
 * @author Gustaf
 * 
 */
public class ConnectController implements Observer {
	private Client client;
	private ConnectPanel panel;
	
	public ConnectController(Client c, ConnectPanel p) {
		client = c;
		panel = p;
		panel.addConnectPanelListener(new ConnectPanelListener());
	}
	
	private class ConnectPanelListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals("Connect") && client.getStatus() == Client.DOWN) {
				client.connectTo(panel.getAddress(), panel.getDisplayName());
			} else if (e.getActionCommand().equals("Disconnect") && client.getStatus() != Client.DOWN) {
				client.disconnect();
			}
		}	
	}

	@Override
	public void update() {
		panel.updateConnectButton(client.getStatus());	
	}
}
