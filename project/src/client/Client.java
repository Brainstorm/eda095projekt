package client;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;

import client.interfaces.*;
import client.sound.Audio;
import protocol.ChatConnection;
import protocol.messages.Handshake;
import protocol.messages.Message;
import protocol.messages.TextMessage;
import protocol.messages.User;

/**
 * Represents the client.
 * Contains methods for connecting to and disconnecting from a server.
 * Contains thread safe methods for sending and retrieving
 * messages. Keeps track of the current state the client is in.
 * @author Gustaf
 * 
 */
public class Client implements Observable {
	public static final int VERSION = 1;
	public static final int DOWN = 0, CONNECTING = 1, CONNECTED = 2, ACTIVECALL = 3;
	public static final int DEFAULTPORT = 3000;
	private int status;
	private Socket socket = null;
	private ClientReceiver clientReceiver = null;
	private ClientSender clientSender = null;
	private ArrayDeque<Message> waitingMessages = null;
	private ArrayList<User> currentUsers = null;
	private ArrayList<Observer> observers;
	private Audio audio;
	private AudioRecorder audioRecorder;
	private ArrayList<TextMessage> chatMessages;

	public Client() {
		audio = new Audio();
		status = Client.DOWN;
		waitingMessages = new ArrayDeque<Message>();
		observers = new ArrayList<Observer>();
		chatMessages = new ArrayList<TextMessage>();
	}
	
	public synchronized void addMessage(Message newMessage) {
		if (getStatus() >= Client.CONNECTED || newMessage.type == Handshake.TYPE) {
			waitingMessages.add(newMessage);
			notifyAll();
		}
	}

	synchronized public Message getNextMessage() {
		while (waitingMessages.size() < 1) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return waitingMessages.remove();
	}
	
	synchronized public void updateCurrentUsers(ArrayList<User> newList) {
		currentUsers = newList;
		notifyObservers();
	}
	
	synchronized public ArrayList<User> getCurrentUsers() {
		return currentUsers;
	}
	
	@SuppressWarnings("deprecation")
	public synchronized void setStatus(int i) {
		status = i;
		if (i == Client.ACTIVECALL) {
			audio.startPlaying(Audio.getRecordingFormat());
			audio.startRecording();
			audioRecorder = new AudioRecorder(this, audio);
			audioRecorder.start();
		} else if (audioRecorder != null) {
			audioRecorder.stop();
			audioRecorder = null;
			audio.stopPlaying();
			audio.stopRecording();
		}
		notifyObservers();
		notifyAll();
	}
	
	synchronized public int getStatus() {
		return status;
	}
	
	synchronized public void connectTo(String host, String displayName) {
		setStatus(Client.CONNECTING);
		String address;
		int port;
		if (host.contains("::")) {
			String[] addressAndPort = host.split("::");
			address = addressAndPort[0];
			port = Integer.parseInt(addressAndPort[1]);
		} else {
			address = host;
			port = Client.DEFAULTPORT;
		}
		try {
			socket = new Socket(address, port);
			addMessage(new Handshake(Client.VERSION, displayName));
			ChatConnection conn = new ChatConnection(socket.getInputStream(), socket.getOutputStream());
			clientReceiver = new ClientReceiver(this, conn.getReceiver(), audio);
			clientReceiver.start();
			clientSender = new ClientSender(this, conn.getSender());
			clientSender.start();
		} catch (IOException e) {
			setStatus(Client.DOWN);
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	synchronized public void disconnect() {
		setStatus(Client.DOWN);
		try {
			clientReceiver.stop();
			clientSender.stop();
			waitingMessages.clear();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	synchronized public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	synchronized public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public synchronized void notifyObservers() {
		for (Observer observer : observers) {
			observer.update();
		}
	}

	public synchronized ArrayList<TextMessage> getNewChatMessages() {
		ArrayList<TextMessage> m = new ArrayList<TextMessage>(chatMessages);
		chatMessages.clear();
		return m;
	}

	public void addChatMessage(TextMessage text) {
		chatMessages.add(text);
		notifyObservers();
	}
}
